const greenquizContainer = document.querySelector('#greenquiz-container');
const quizMain = document.querySelector('#quiz-main');
const form = document.querySelector('#quiz-form');
const title = document.querySelector('#question-title');
const timer = document.querySelector('#timer');
const progressBar = document.querySelector('.progress-bar');
const questionIndicator = document.querySelector('#question-indicator');

var points = 0; // correct answers
var currentQuestion = 0;




const questions = [
    {
        title: "In Bulgaria, 90% of home-based workers are women", answer: true
    },
    {
        title: "Sweatshop Workers get screamed at for going to the bathroom, even when working 10 hour shifts", answer: true
    },
    {
        title: "Sometimes women are contractually obliged not to become pregnant for 3 years.", answer: true
    },
    {
        title: "In many countries, child banana workers only earn a half of the minimum wage.", answer: false, correction: "they actually only earn a third of the minimum wage."
    },
    {
        title: "More girls are trapped in child labour than boys.", answer: false, correction: "about 100 million boys are trapped in child labour, whereas about 68 million girls are trapped in child labour."
    },
    {
        title: "Around three quarters of all children trapped in child labour are engaged in hazardous work.", answer: false, correction: "– actually around one half."
    },
    {
        title: "40% of all employed adolescents aged 15 to 17 years are in hazardous work.", answer: true
    },
    {
        title: "800 women die every day from causes linked to pregnancy, childbirth or postpartum.", answer: true
    },
    {
        title: "About 20% of garment workers are unionised.", answer: false, correction: "– only between 5 and 10% of workers are unionised, and most of these are in the ‘yellow’ unions established by factory managements."
    },
    {
        title: "50% of Bangladeshi garment workers work from 8am until 8pm or 10pm.", answer: false, correction: "– it’s actually 80%."
    },
    {
        title: "Garment workers can be dismissed if they refuse to work overtime.", answer: true
    },
    {
        title: "Worldwide, one out of every ten deaths among children under the age of five is due to a water-related disease.", answer: false, correction: " Actually one out of every five deaths."
    },
    {
        title: "Almost two thirds of households rely on women to fetch the family’s water when there is no water source at home.", answer: true
    },
    {
        title: "Worldwide, about 60 to 75 million people are employed in the clothing, footwear and textile industry.", answer: true
    },
    {
        title: "In 2000, only 20 million people were employed in the clothing, footwear and textile industry worldwide.", answer: true
    },
    {
        title: "According to the report ‘Global Wage Trends for Apparel Workers, 2001-2011’, Vietnamese garment workers’ wages were only 22% of the living wage.", answer: true
    },
    {
        title: "When female workers fail to meet impossible targets, they have to suffer verbal abuse: for instance they can be called “dogs and donkeys” and to “go and die”.", answer: true
    },
    {
        title: "There is a global minimum wage.", answer: false
    },
    {
        title: "Fashion is the most polluting industry in the world.", answer: false, correction: " is the second most polluting industry (after oil) in terms of its environmental impact."
    },
    {
        title: "In Uzbekistan, forced labour among adults is common as well as forced labour among children. Therefore, in schools across the country 50% of teachers are absent at any given time.", answer: true
    },
    {
        title: "A reduction in biodiversity will have a minimal impact on human populations", answer: false
    },
    {
        title: "Activities that lead to a reduction in biodiversity occur only in less industrialised countries.", answer: false
    },
    {
        title: "Strategies that are important for developing a sustainable world are to consume less, recycle more, lower the worldwide fertility rates and reduce rural world poverty.", answer: true
    },
    {
        title: "Eutrophication is the rapid growth of plant life and the death of animal life in a shallow body of water as a result of excessive nutrients.", answer: true
    },
    {
        title: "Only organic nutrients contribute to water pollution.", answer: false
    },
    {
        title: "Ozone near the Earth’s surface is beneficial, but ozone in the stratosphere is considered pollution.", answer: false
    }
]







// render the question on screen with the true or false options
function renderQuestion(question) {
    let progressPerc = (currentQuestion / questions.length) * 100;
    progressBar.style.width = progressPerc + "%";
    progressBar.setAttribute('aria-valuenow', progressPerc);


    if(currentQuestion > questions.length-1){
        finalizar_workshop(1,1,questions);
    }else{
        title.textContent = question.title;
        document.querySelector('#radio-true').checked = false;
        document.querySelector('#radio-false').checked = false;
        quizMain.textContent = '';
        quizMain.appendChild(greenquizContainer);
        greenquizContainer.style.display = 'block';
        questionIndicator.textContent = 'Question ' + (currentQuestion+1) + '/' + questions.length;    
    }
}
// event next question
form.addEventListener('submit', (e) => {
    e.preventDefault();
    //getAnswers(currentQuestion);
    currentQuestion++;
    renderQuestion(questions[currentQuestion]);
    setResetTime();
    
   
});

//renderQuestion(questions[0]);


// start quiz
function startQuiz(e) {
    renderQuestion(questions[0]);
}

// Save answers

function getAnswers(j){
var respostas = null;


    if(document.getElementById(radio-true).checked)
    {
        if(resposta == null)
            resposta= j + "-true";
        else
        resposta=resposta +";" + j +"-true"
    }
    else{
         if(document.getElementById(radio-false).checked)
         if(resposta == null)
         resposta= j + "-false";
        else
     resposta=resposta +";" + j +"-false"
 }

                alert(resposta);



};
