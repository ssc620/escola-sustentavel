const greenquizContainer = document.querySelector('#greenquiz-container');
const respostas = document.querySelector('#respostas');
const quizMain = document.querySelector('#quiz-main');
const form = document.querySelector('#quiz-form');
const title = document.querySelector('#question-title');
const timer = document.querySelector('#timer');
const progressBar = document.querySelector('.progress-bar');
const questionIndicator = document.querySelector('#question-indicator');

var points = 0; // correct answers
var currentQuestion = 0;




const questions = [
    {
        title: "Workers in the garment industry should be grateful for the jobs fashion companies provide.", answer: true
    },
    {
        title: "I would pay more for my clothes if the workers got a better wage. ", answer: true
    },
    {
        title: "Homeworking is better for women than working in a factory. ", answer: true
    },
    {
        title: "Low pay is fine for workers in developing countries because the cost of living is lower there. ", answer: false
    },
    {
        title: "If children are found working in a factory, the factory should be closed. ", answer: false
    },
    {
        title: "Trade unions are a bad influence on workers and simply cause trouble. ", answer: false
    },
    {
        title: "It is entirely companies’ responsibility to make sure that the workers who make their clothes are treated fairly.", answer: false
    },
    {
        title: "It is better to buy clothes made in Britain because it protects British jobs.", answer: false
    },
    {
        title: "It is better to buy garments from China because it creates jobs for Chinese people.", answer: false
    },
    {
        title: "The workers who make clothes really do not worry me.My concern is that I look good and can afford to buy the latest fashion.", answer: false
    },
    {
        title: "It is better to buy clothes from independent retailers than from high street chains.", answer: false
    },
    {
        title: "To create a more sustainable environment industry should only exist in highly technological countries", answer: false
    },
    {
        title: "If children in sweatshops are not working they will certainly be in school so that they can go to uni.", answer: false
    }

]







// render the question on screen with the true or false options
function renderQuestion(question) {
    let progressPerc = (currentQuestion / questions.length) * 100;
    progressBar.style.width = progressPerc + "%";
    progressBar.setAttribute('aria-valuenow', progressPerc);

    if(currentQuestion ==  questions.length){
        finalizar_atividade(1,1,questions);
        $("#greenquiz-container").fadeOut();
        
        respostas.style.display = 'block';

        questions.forEach(function(element) {
            if(element.answer)
            $("#lista-resposta").append(' <li class="list-group-item list-group-item-success">'+element.title+'</li>');
            else
            $("#lista-resposta").append(' <li class="list-group-item list-group-item-danger">'+element.title+'</li>');
        });
        
    }else{
        title.textContent = question.title;
        document.querySelector('#radio-true').checked = false;
        document.querySelector('#radio-false').checked = false;
        quizMain.textContent = '';
        quizMain.appendChild(greenquizContainer);
        greenquizContainer.style.display = 'block';
        questionIndicator.textContent = 'Question ' + (currentQuestion+1) + '/' + questions.length;    
    }
}
// event next question
form.addEventListener('submit', (e) => {
    e.preventDefault();
    //getAnswers(currentQuestion);
    currentQuestion++;
    renderQuestion(questions[currentQuestion]);
    setResetTime();   
});

//renderQuestion(questions[0]);


// start quiz
function startQuiz(e) {
    renderQuestion(questions[0]);
}

// Save answers

function getAnswers(j){
var respostas = null;


    if(document.getElementById(radio-true).checked)
    {
        if(resposta == null)
            resposta= j + "-true";
        else
        resposta=resposta +";" + j +"-true"
    }
    else{
         if(document.getElementById(radio-false).checked)
         if(resposta == null)
         resposta= j + "-false";
        else
     resposta=resposta +";" + j +"-false"
 }




};
