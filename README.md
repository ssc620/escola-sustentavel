# Escola Sustentável

Esse projeto foi criado por alunos da Engenharia de Computação para a disciplina
de Engenharia de Software.

Para utilizar localmente o site, basta *clonar* o repositório na pasta onde o
seu servidor Apache serve os arquivos. Então, basta acessar
*localhost:XXXX/escola-sustentavel/interface*, onde XXXX é sua porta
configurada.
